
#!/bin/bash
: '
Update OSX
Update XCode
Open XCode & Accept the terms and conditions
Install latest brew: https://brew.sh/
Run this shell: ./setupOSX
'

# setup mosquitto
sudo xcode-select --reset
sudo chown -R $(whoami) $(brew --prefix)/*
brew install mosquitto
brew upgrade mosquitto
launchctl load /usr/local/Cellar/mosquitto/*/homebrew.mxcl.mosquitto.plist
launchctl start homebrew.mxcl.mosquitto

# setup pcap
brew install wget
./pcapInstall

# clone the public repository
git clone https://wavelite_public@bitbucket.org/wavelite_public/wavelite-software.git

# setup nodejs modules
cd wavelite-software/nodejs
npm install

# go to exe dir
cd ../wavelite
