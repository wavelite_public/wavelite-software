#!/bin/bash
# Created by Pooya Merat in Jun 2018.
# Copyright © 2018 Wavelite. All rights reserved.

: '
Downloads, makes and installs PCAP library
'

unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
	echo installing flex and bison
	apt-get -y install flex
	apt-get -y install bison
fi

echo "=== Downloading libpcap-1.8.1 ==="
cd ..
mkdir pcap_src
cd pcap_src
wget http://www.tcpdump.org/release/libpcap-1.8.1.tar.gz
echo "=== Unpacking pcap ==="
tar -xf libpcap-1.8.1.tar.gz
cd libpcap-1.8.1
echo "=== Configuring pcap ==="
./configure
echo "=== Making and Installing pcap ==="
# Install the pcap library in: usr/local/lib
make && make install

# Refresh the paths
if [[ "$unamestr" == 'Linux' ]]; then
	ldconfig
fi

cd ../..
rm -r pcap_src
