#!/bin/bash
# Created by Pooya Merat in Jun 2018.
# Copyright © 2018 Wavelite. All rights reserved.

: '
============ Linux setup instructions ============

 <<<< [For a linux PC jump to step 6. Steps 1-5 are basic setup for raspberry pi] >>>>

1. Download Raspbian and Etcher:
	a. The latest Raspbian LITE image from: https://www.raspberrypi.org/downloads/raspbian/
	b. Etcher from: https://www.balena.io/etcher/

2. Clone:
	Use Etcher to clone the latest Raspbian on an SD card (takes 2~5 minutes).

3. Set up the hardware:
	a. Insert the SD card to a raspberry pi
	b. Connect a monitor through the HDMI port to the raspberry pi
	c. Connect a USB Keyboard
	d. Power on the raspberry pi

4. Login to Raspbian:
	login: pi
	pass: raspberry

5. Internet connection:
  (a). Either connect a LAN cable to raspberry pi
	(a). OR set up WiFi by running raspi-config and selecting
		 Network Options >> WiFi and enter country, SSID and password.
     You may need to restart the raspberry pi.
  b. Check the connection by pinging 8.8.8.8:
			ping 8.8.8.8 -c 3

6. Clone the set up files:
	# Run this command [on linux PC: followed by the password]
		sudo su
	# Install git:
		apt-get install -y git
	# Go to root dir
		cd /root/
	# Clone the public repository
		git clone https://wavelite_public@bitbucket.org/wavelite_public/wavelite-software.git

7. Install all required packages:
	# Go to the setup dir
		cd wavelite-software/setup
	# Run the setup shell to set up linux for wavelite
    chmod u+x ./setupLinux.sh
		./setupLinux.sh hostname=HOST-NAME
	#	Wait for the setup to complete, lookout for any errors or check the setup.log in the /setup dir.
  # The password for users "pi" and "root" is both now set to: "wavelite"

8. [Optional] From another computer on the same network SSH to this machine:
	a. ssh root@HOST-NAME.local
	b. enter "wavelite" when prompted for password
'

setup_server=true;
dev=true; # if set to false only the public repo will be cloned

# default hostname
hostname="wave-linux"

# Verift OS
case "$OSTYPE" in
  linux*)
      printf "OS: %s detected.\n" $OSTYPE ;;
  *)
      printf "\033[1;31m \tThis script can only be used for a linux machine.\n\tUnsupported OS detected: %s \033[0m\n" $OSTYPE;
      exit ;;
esac

# parse inputs
for argument; do
    key=${argument%%=*}
    value=${argument#*=}

    case "$key" in
            hostname)   hostname=$value ;;
            dev)        dev=$value ;;
            *)          printf "\033[1;31m Unknwon command: $key \033[0m\n"; exit ;;
    esac
done

Title_Color='\033[1;46m';
NC='\033[0m'; # No Color
print_title() { printf "${Title_Color}  ======== $1 ====== ${NC}\n"; };

# Run and log the setup file
{
  print_title " Setting up wavelite transceiver on Linux! "
  date -d now

  print_title "Update"
  apt-get update

# Only setup the hostname and password on a raspberry pi
if [ "$OSTYPE" = "linux-gnueabihf" ]; then
  print_title "Setting the Host-Name"
  ./setHostName.sh $hostname

  print_title "Setting the Password"
  echo root:wavelite | chpasswd
  echo pi:wavelite | chpasswd
  print_title "Setting Up Open SSH"
  aptitude install -y openssh
fi

  systemctl enable ssh
  sed -i -E 's/(#)?PermitRootLogin .*/PermitRootLogin yes/' /etc/ssh/sshd_config
  service sshd restart

  print_title "Installing Git"
  apt-get install -y git

  print_title "Installing PCAP"
  ./pcapInstall.sh

  print_title "Installing lshw"
  apt-get install -y lshw

if [ setup_server ]; then
  print_title "Installing and Setting up MOSQUITTO"
  apt-get install -y aptitude
  aptitude install -y mosquitto
  aptitude install -y libmosquitto-dev
  systemctl start mosquitto
  systemctl enable mosquitto

  print_title "Installing Node JS"
  apt-get install -y curl
  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  apt-get install -y nodejs

  # setup nodejs modules
  cd /root/wavelite-software/nodejs
  npm install
fi

  # go to exe dir
  cd ../wavelite

  # make the exe executable
  chmod u+x ./run-wave-linux

} 2>&1 | tee setup.log
