# How to Setup System for Wavelite Software

The system consists of a transmitter, receiver and a tag. Below is the setup directions for the transmitter and the receiver.

## Ambient Transmitter Using Raspberry Pi
Log in to the RPI router with the following log in information:

`Username: root`
`Password: wavelite`

Make sure the following command is running on your RPI transmitter that is simulating an off-the shelf WiFi router:

`lorcon_examples/beacon_flood_lcpa_add -s wavelite_router -i wlan0 -c 1`
	  
## Ambient Transmitter Using Off-the shelf router

Log in to your router's Configuration GUI and take a note of Channel number and its MAC address.
pre-setting the channel to channel 1 or 3 is recommended.
You can later use the channel number and the mac address in Sections __How to run Wavelite Software: On Raspberry Pi (Linux)__  and  __How to run Wavelite: Software On Mac OS X (Darwin)__.

## Receiver On Raspberry Pi (Linux)

If you do not have a Wavelite SD card please refer to `setup/setupLinux` and then continue to __How to run Wavelite Software: On Raspberry Pi (Linux)__ below.

## Receiver On Mac OS X (Darwin)

Please follow setup instructions in `setup/setupOSX` and then continue to __How to run Wavelite Software: On Mac OS X (Darwin)__ below.


# How to Run Wavelite Software 

## On Raspberry Pi (Linux)

1. Setup the hardware using one of the following methods:
	1. With a USB Keyboard and an HDMI Monitor:
		1. Enter the Wavelite SD card to a raspberry pi
		1. Connect a monitor with HDMI to the raspberry pi
		1. Connect a USB Keyboard
		1. Make sure the Eastech WiFi usb adaptor is disconnected
		1. Power on the raspberry pi
		1. Logon to the system
`Username: root`
`Password: wavelite`

	1. Using ssh connection:
		1. Enter the Wavelite SD card to a raspberry pi
		1. Make sure the Eastech WiFi usb adaptor is disconnected
		1. Power on the raspberry pi
		1. Logon to the system
			`ssh root@wave-pi3r.local`
		1. enter `wavelite` when prompted for password

1. Go to the executable directory: `cd wavelite-software/wavelite`.
1. Plugin the Eastech WiFi usb adaptor to the Raspberry Pi.
1. Run the following command:

	`./run-wave-linux –c “channel number of the ambient transmitter” -m  “mac address of the ambient transmitter”`

	_Example_: `./run-wave-linux –c 3 -m 00:de:ad:be:ef:00`
  
1. In the raspberry pi terminal, you will observe the program being executed.

1. Note down the URL address of Wavelite’s dashboard (NODE: Local website address: WEB_ADDRESS) and copy the WEB_ADDRESS in the browser of an arbitrary device in the same local network, you’ll be able to observe the temperature and humidity data on the web dashboard.

## On Mac OS X (Darwin)

1. Open up ___two___ terminal windows in your mac device
1. In the first terminal `cd wavelite-software/wavelite` and run the following command:

	`sudo ./run-wave-darwin –c “channel number of the ambient transmitter” -m  “mac address of the ambient transmitter”`

	_Example_: `sudo ./run-wave-darwin –c 3 -m 00:de:ad:be:ef:00`

1. In the second terminal `cd wavelite-software/wavelite/nodejs` and run `node server.js`

1. Note down the URL address of Wavelite’s dashboard (NODE: Local website address: WEB_ADDRESS) and copy the WEB_ADDRESS in your mac's browser, you’ll be able to observe the temperature and humidity data on the Dashboard

# Additional Notes
* `./run-wave-linux -h` will list you the available optional arguments.

# Known issues
* In case of using ssh connection to raspberry pi, make sure the WiFi dongle (Eastech) is not connected, otherwise once you run the executable, the ssh connection will be lost. 
