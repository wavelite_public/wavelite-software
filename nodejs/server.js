

var mqtt_server_ip_addr = 'localhost';

var port_offset = 40510;

var os = require("os");
var hostname = os.hostname();
var web_server_addr = hostname.split('.local')[0] + '.local';

// =================== WebServer ===================

var express = require('express');

//Express server setup
var app = express();

app.use('/', express.static(__dirname + '/www'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

var server_port = 3000;

app.listen(server_port, web_server_addr, function () {
  console.log('NODE: Local website address: ' + web_server_addr + ":" + server_port + '/?port=' + port_offset + ',' + (port_offset+1) + '#');
});

// =================== Websockets ===================

const WebSocket = require('ws')
const WebSocketServer = WebSocket.Server;

var args = process.argv.slice(2);

var connections = [];
var servers = [];

for(var k = 0; k < 1 * 2; k++){ // Assuming two ports for each tag
  connections[k] = [];
  servers[k] = new WebSocketServer({ port: port_offset + k, autoAcceptConnections: false});
  servers[k].on('connection', function (conn) {
    var conn_port = conn._socket._server._connectionKey.slice(5);
    connections[conn_port - port_offset].push(conn);
    console.log('NODE: Module ', conn_port, ' connected.');
    conn.on('message', function (message) {console.log( 'NODE: ' + conn_port + ' received: ', message);});
    conn.on('close', function() {console.log('NODE: ' + conn_port + ' monitoring stopped');});
    conn.on('error', function() { console.log('NODE: ' + conn_port + ' ERROR');});
  });
}

function send_msg(port_id, msg){
  for (var i = 0; i < connections[port_id].length; i++)
    if (connections[port_id][i] != null && connections[port_id][i].readyState == WebSocket.OPEN)
      connections[port_id][i].send(msg);
}

// =================== MQTT Stuff ===================

var mqtt = require('mqtt');

var mqttbroker = mqtt_server_ip_addr + ':1883';
var mqttclient = mqtt.connect('mqtt://' + mqttbroker, {
  username: 'wave',
  password: 'lightgrow'
});

mqttclient.on('connect', function() {
  console.log('NODE: ' + 'MQTT client connected!');
});

mqttclient.subscribe('wl/#');

mqttclient.on('message', function(topic, payload) {

  sub_topic = topic.split("/")[1];

  if (payload.length != 8) return;

  var tempr = payload.readFloatLE(0);
  var humid = payload.readFloatLE(4);

  //console.log('NODE: ' + topic + ' : ' + tempr.toFixed(2) + ', ' + humid.toFixed(2));

  send_msg(0, tempr);
  send_msg(1, humid);

});

//mqttclient.publish('good_topic', 'msg');
