
var server_address = document.location.host.split(":")[0];

var port_offset = 40510;

//limits number of modules
var maxModules = 4;

// Smootie globals
var line = [];
var line_cols = ["rgb(204, 51, 51)", "rgb(204, 166, 51)", "rgb(89, 204, 51)", "rgb(51, 204, 204)", "rgb(51, 51, 204)", "rgb(204, 51, 204)"];
var smoothie_container = [];
var smoothie_canvas = [];

var y_min = [20, 40];
var y_max = [40, 70];

//module array containing module info
var module = [];

addModuleFromURL(getParameterByName('port'));
//Setup module array containing module info from localStorage
if (JSON.parse(localStorage.getItem("module")) !== 'undefined' && JSON.parse(localStorage.getItem("module")) !== null)
{
    module = JSON.parse(localStorage.getItem("module"));
    updateConfig();
    connectAll();
}
else
{
    document.getElementById("welcome").style.display = "block";
}

function resizeSmoothie(id) {
  console.log('Resizing smoothie: ' + id);
  smoothie_container[id].style.height =  (0.25 * smoothie_container[id].offsetWidth).toString() + 'px';
  smoothie_canvas[id].width = smoothie_container[id].offsetWidth;
  smoothie_canvas[id].height = smoothie_container[id].offsetHeight;
};

function resize_graphs(){
  resizeSmoothie(0);
  resizeSmoothie(1);
}

function connect(id) {
    // module[id].ws = new WebSocket("ws://' + server_address + ':"+module[id].port);
    // module[id].ws = new WebSocket("ws://127.0.0.1:"+module[id].port);
    module[id].ws = new WebSocket("ws://"+server_address+":"+module[id].port);

    document.getElementById("welcome").style.display = "none";
    document.getElementById("refresh").style.display = "block";


    console.log(id);
    // elements
    smoothie_container[id] = document.getElementById("smoothie-container"+id);
    smoothie_canvas[id] = document.getElementById("smoothie-canvas"+id);

    // resize
    window.onresize = resize_graphs;

    // Create a Chart
    var chart = new SmoothieChart({
      millisPerPixel:20,
      limitFPS:25,
      lineWidth:3,
      minValue:y_min[id],
      maxValue:y_max[id],
      // timestampFormatter:,
      interpolation: 'linear', // 'bezier', 'linear'
      maxValueScale:1.15,
      // scaleSmoothing:0.984,
      labels:{fontFamily:'Arial'},
      grid:{verticalSections:5,
        fillStyle:'#ffffff',strokeStyle:'#80ccff',borderVisible:false},labels:{fillStyle:'#000000'},
        // tooltip:true, // Not pleasant!
        timestampFormatter:SmoothieChart.timeFormatter
      });

    chart.streamTo(smoothie_canvas[id]);

    // chart.streamTo(document.getElementById('chart'+id), 500);
    line[id] = new TimeSeries();
    chart.addTimeSeries(line[id], {lineWidth:3, strokeStyle:line_cols[id],fillStyle:line_cols[id].replace(')', ', 0.3)').replace('rgb', 'rgba')});

    //display card
    var cardName = "card"+id;
    var thisCard = document.getElementById(cardName);
    if (thisCard.style.display === "none") {
        thisCard.style.display = "block";
    }
    document.getElementById("connectionStateDiv"+id).innerHTML='<small class="text-muted">Connecting...</small>';
    var divName = "smoothie-canvas"+id;
    var thisPlot = document.getElementById(divName);
    thisPlot.style.display = "none";

    if (module[id].description != 'undefined'){
                document.getElementById("description"+id).innerHTML = capitalizeFirstLetter(module[id].description);
    }
    else{
        document.getElementById("description"+id).innerHTML = '';
    }

    module[id].ws.onopen = function () {
        console.log('module '+ id +' is broadcasting ...');
        // module[id].ws.send('monitoring ...');
        module[id].lastOnline = new Date().toUTCString();
        localStorage.setItem("module", JSON.stringify(module));
        document.getElementById("connectionStateDiv"+id).innerHTML='<small class="text-muted">Real-time</small>';
        document.getElementById("smoothie-canvas"+id).style.display = "block";
        updateConfig();
    }

    module[id].ws.onmessage = function (event) {

        line[id].append(new Date().getTime(), event.data);

        var port_id = module[id].port - port_offset;
        //Application specific
        if        (port_id == 0) document.getElementById("tempSpan").innerHTML = parseFloat(event.data).toFixed(2);
        else if   (port_id == 1) document.getElementById("humiSpan").innerHTML = parseFloat(event.data).toFixed(1);
        else if    (port_id == 2) document.getElementById("tempSpan2").innerHTML = parseFloat(event.data).toFixed(2);
        else if   (port_id == 3) document.getElementById("humiSpan2").innerHTML = parseFloat(event.data).toFixed(1);
        // elseif  console.log("bad sensor id");

        // elseif  (temp_id == 2) document.getElementById("humiSpan").innerHTML = Math.round(event.data);
        // elseif  (temp_id == 3) document.getElementById("humiSpan").innerHTML = Math.round(event.data);
    }

    module[id].ws.onclose = () => {
        if (module.length !== 0) {
            console.log( 'module '+ id +' is not broadcasting');
            document.getElementById("connectionStateDiv"+id).innerHTML='<small class="text-muted">Offline</small>';
            document.getElementById("smoothie-canvas"+id).style.display = "none";
        }
        updateConfig();
    }
}

function connectAll(){
    if (module.length == 0) {
        document.getElementById("welcome").style.display = "block";
        document.getElementById("refresh").style.display = "none";
    }
    for (var id = 0; id < module.length; id++) {
        connect(id);
    }
    document.getElementById("configuredModuleCounter").innerHTML = module.length;
    updateConfig();
}

function refreshAll(){
    if (module.length == 0) {
        document.getElementById("welcome").style.display = "block";
        document.getElementById("refresh").style.display = "none";
    }
    for (var id = 0; id < module.length; id++) {
        if(module[id].ws.readyState === module[id].ws.CLOSED){
            connect(id);
        }
    }
    document.getElementById("configuredModuleCounter").innerHTML = module.length;
    document.getElementById("inputPort").innerHTML = "";
}

function updateConfig() {
    //update module config from localStorage JSON
    var myTable = document.getElementById("myConfig");
    myTable.innerHTML = "";
    if (module.length){
        for (var i = 0; i < module.length; i++)
        {
            var newRow = document.getElementById("myConfig").insertRow(-1);
            newRow.innerHTML = "<td>"+i+"</td><td>"+module[i].port+"</td><td>"+module[i].description+"</td><td>"+module[i].ws.readyState+"</td><td onclick=\"deleteModule(this)\"><a href=\"#\">&times;</a></td>";
        }
        document.getElementById("configuredModuleCounter").innerHTML = module.length;
    }
}

function addModule() {
    var entry = document.getElementById("inputPort").value;
    var res = entry.split(" ");
    for (var i = 0; i < res.length; i++) {
        //limiting number of configured modules to maxModules
        //verifies if port is a number, in range and not already configured
        if (module.length < maxModules && checkInp(res[i]) && checkIfAlreadyConfigured(res[i])) {
            var moduleObj = {
                port: res[i],
                description: 'undefined',
                lastOnline: 'undefined',
                ws: undefined,
                module_data: [],
                time_data: [],
                data: []
            };
            module.push(moduleObj);
            connect(module.length - 1);
            refreshAll();
        }
    }
    document.getElementById('inputPort').value='';
    document.getElementById("confirmClearDisp").innerHTML = '';
    localStorage.setItem("module", JSON.stringify(module));
    document.getElementById("configuredModuleCounter").innerHTML = module.length;
    updateConfig();
}

function addModuleFromURL(x){
    if (x != null) {
        var inputArgPort = x.split(',');
        for (var i = 0; i < inputArgPort.length; i++) {
            if (module.length == maxModules || !checkIfAlreadyConfigured(inputArgPort[i]) || isNaN(inputArgPort[i]) || inputArgPort[i]<40500 || inputArgPort[i]>40800){
                return 0;
            }
            else {
                var moduleObj = {
                    port: inputArgPort[i],
                    description: 'undefined',
                    lastOnline: 'undefined',
                    ws: undefined,
                    module_data: [],
                    time_data: [],
                    data: []
                };
                module.push(moduleObj);
                connect(module.length-1);
                refreshAll();
                document.getElementById("configuredModuleCounter").innerHTML = module.length;
                localStorage.setItem("module", JSON.stringify(module));
                updateConfig();
            }
        }
    }
}

function confirmClear(){
    if (module.length !=0) {
        document.getElementById("confirmClearDisp").classList.add('text-danger');
        document.getElementById("confirmClearDisp").innerHTML = " Confirm...";
    }
}

function clearAllModalDisp(){
    document.getElementById("confirmClearDisp").innerHTML = '';
    document.getElementById("inputError").innerHTML = '';
    document.getElementById("inputPort").innerHTML = '';
}

function clearCofig() {
    if (module.length){
        for (var i = 0; i < module.length; i++)
        {
            document.getElementById("myConfig").deleteRow(0);
            module[i].ws.close();
            console.log( 'communication with module '+ i + ' stopped');
            document.getElementById("description"+i).innerHTML = '';
        }
        document.getElementById("configuredModuleCounter").innerHTML = 0;
        localStorage.clear();
        document.getElementById("confirmClearDisp").classList.remove('text-danger');
        document.getElementById("confirmClearDisp").classList.add('text-success');
        document.getElementById("confirmClearDisp").innerHTML = " DONE";
        document.getElementById("welcome").style.display = "block";
        document.getElementById("refresh").style.display = "none";

        for (var id = 0; id < maxModules; id++) {
            var cardName = "card"+id;
            var thisCard = document.getElementById(cardName);
            thisCard.style.display = "none";
        }
    }
}

function checkInp(x){
  if (isNaN(x) || x<40500 || x>40800)
  {
    document.getElementById("inputError").innerHTML ="Must input port numbers within [40500, 40800]";
    return false;
  }
  return true;
}

function checkIfAlreadyConfigured(x){
    for (var i = 0; i < module.length; i++) {
        if (module[i].port == x){
            return false;
        }
    }
    return true;
}

function closeWindow(x){
    var windowId = "card"+x;
    document.getElementById(windowId).style.display = "none";
    module[x].ws.close();
}

function updateDescription(x){
    document.getElementById('ModuleModalLabel').innerHTML = "Module "+x+" description";
    document.getElementById('modulePort').innerHTML = module[x].port;
    document.getElementById("moduleLastOnline").innerHTML = module[x].lastOnline;
    document.getElementById('moduleDescription').innerHTML = module[x].description;
    var funcName = "applyUpdateDescription("+x+");";
    updateDescriptionId.setAttribute("onclick", funcName);
}

function applyUpdateDescription(x){
    if (document.getElementById("inputNewDescription").value != '')
    {
        module[x].description = capitalizeFirstLetter(document.getElementById("inputNewDescription").value);
        document.getElementById('moduleDescription').innerHTML = capitalizeFirstLetter(module[x].description);
        document.getElementById("description"+x).innerHTML = capitalizeFirstLetter(module[x].description);
        document.getElementById("inputNewDescription").value='';
        localStorage.setItem("module", JSON.stringify(module));
        updateConfig();
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function deleteModule(x) {
    id = x.parentNode.rowIndex - 1;
    for (var i = 0; i < module.length; i++)
    {
        module[i].ws.close();
        document.getElementById("description"+i).innerHTML = '';
        var cardName = "card"+i;
        var thisCard = document.getElementById(cardName);
        thisCard.style.display = "none";
    }
    module.splice(id, 1);
    console.log( 'communication with module '+ id + ' stopped');
    localStorage.setItem("module", JSON.stringify(module));
    connectAll();
    updateConfig();
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//Application specific JS

function humidity() {
    var x = document.getElementById("humidity");
    var y = document.getElementById("temperature");
    y.style.display = "none";
    x.style.display = "block";
}

function temperature() {
    var x = document.getElementById("temperature");
    var y = document.getElementById("humidity");
    y.style.display = "none";
    x.style.display = "block";
}

function showDashboard() {
    var x = document.getElementById("dashboard");
    var y = document.getElementById("application");
    y.style.display = "none";
    x.style.display = "block";
    resize_graphs();
}

function showApplication() {
    var x = document.getElementById("application");
    var y = document.getElementById("dashboard");
    y.style.display = "none";
    x.style.display = "block";
}
